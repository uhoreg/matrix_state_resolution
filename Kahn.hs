{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}

module Kahn
where

import GHC.Generics (Generic)
import Control.Monad (guard)
import Data.Foldable (foldl', minimumBy)
import qualified Data.HashMap.Strict as Map
import qualified Data.HashSet as Set
import qualified Data.List as List

-- Implementation of Kahn's algorithm, by Matt Mullen

data Node a =
  Node { label :: String
       , entry :: a
       } deriving (Eq, Show, Generic)

data Edge =
  Edge { from :: String
       , to :: String
       } deriving (Eq, Show, Generic)

data Graph a =
  Graph { entries :: Map.HashMap String a
        , outbound :: Map.HashMap String (Set.HashSet String)
        , inbound :: Map.HashMap String (Set.HashSet String)
        } deriving (Eq, Show, Generic)

node :: String -> Graph a -> Maybe (Node a)
node lbl Graph{..} = Node lbl <$> Map.lookup lbl entries

nodes :: Graph a -> [Node a]
nodes = map (uncurry Node) . Map.toList . entries

isTop :: Node a -> Graph a -> Bool
isTop (Node label _) Graph{inbound} = not $ Map.member label inbound

tops :: Graph a -> [Node a]
tops g = filter (`isTop` g) (nodes g)

empty :: Graph a
empty = Graph Map.empty Map.empty Map.empty

makeGraph :: (Foldable f)
          => (a -> String)
          -> (a -> [String])
          -> f a
          -> Graph a
makeGraph mkLabel mkOutbound = foldl' f empty
  where
    f Graph{..} next = Graph entries' outbound' inbound'
      where
        nextLabel = mkLabel next
        nextOutbound = Set.fromList $ mkOutbound next
        entries' = Map.insert nextLabel next entries
        outbound' = if Set.null nextOutbound
          then outbound
          else Map.insert nextLabel nextOutbound outbound
        addInbound = maybe (Just $ Set.singleton nextLabel) (Just . Set.insert nextLabel)
        inbound' = Set.foldl' (flip (Map.alter addInbound)) inbound nextOutbound

prune :: Edge -> Graph a -> Graph a
prune Edge{from,to} Graph{..} = Graph entries outbound' inbound'
  where
    fromOut = Map.lookupDefault Set.empty from outbound
    toIn = Map.lookupDefault Set.empty to inbound
    (fromOut', toIn') = if Set.member to fromOut
      then (Set.delete to fromOut, Set.delete from toIn)
      else (fromOut, toIn)
    outbound' = if Set.null fromOut'
      then Map.delete from outbound
      else Map.insert from fromOut' outbound
    inbound' = if Set.null toIn'
      then Map.delete to inbound
      else Map.insert to toIn' inbound

pruneFrom :: Node a -> Graph a -> Maybe (Graph a, [Node a])
pruneFrom from g = run <$> toNodes
  where
    toNodes = traverse (`node` g)
            . Set.toList
            . Map.lookupDefault Set.empty (label from)
            $ outbound g
    run tos = go g tos []

    go g [] acc = (g, acc)
    go g (to:xs) acc = go g' xs acc'
      where
        g' = prune (Edge (label from) (label to)) g
        acc' = if isTop to g' then to:acc else acc

kahn :: (a -> a -> Ordering) -> Graph a -> Maybe [a]
kahn ordering g = go g sortedTops []
  where
    ordering' a b = ordering (entry a) (entry b)

    sortedTops = List.sortBy ordering' $ tops g

    validate Graph{outbound} = Map.null outbound

    insert = List.insertBy ordering'
    insertAll xs ys = foldr insert ys xs

    go g [] acc = reverse acc <$ guard (validate g)
    go g (x:xs) acc = do
      (g', newTops) <- pruneFrom x g
      go g' (insertAll newTops xs) (entry x:acc)
